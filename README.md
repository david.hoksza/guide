# Guide

The guide plugin provides introductory guide through various aspects of the MINERVA platform.


### General instructions

In order to use the precompiled and publicly available version of the plugin, 
open the plugin menu in the MINERVA's upper left corner (see image below) and click plugins.
In the dialog which appears enter the following address in the URL box: 
`https://minerva-dev.lcsb.uni.lu/plugins/guide/plugin.js` .
The plugin shows up in the plugins panel on the right hand side of the screen.

![printscreen](img/plugin.jpg)

### Plugin functionality

When the plugin is uploaded into MINERVA, it loads a list of *stories*.
Each story consists of a number of steps list of which can be revealed by clicking on the arrow 
next to the story description. 
The walk-through starts with clicking on the story name and guides the user through the sequence of its steps.
After each step, the panel with description of the corresponding step changes its color to indicate which
steps the user has already been through. At any time, the user can click anywhere in the application
to terminate the process. If the user wants to return to the story at the step where she last interrupted the
process, she can simply click on given step and will be taken directly to the corresponding point in the story.

### Definition of stories
The plugin is using the JavaScript library [Intro.js](https://introjs.com) to specify the stories
 and its individual steps. The stories need to be hard-coded in the plugin (see the `index.js` file).
 Each story (introduction in the *Intro.js* language) consists of a sequence of steps each of which 
 is defined by a CSS selector and a text. Each steps also triggers an event which can be used to 
 interact (from JavaScript) with the map. Using this mechanism, the plugin can simulate user actions
 guiding through the application.
 