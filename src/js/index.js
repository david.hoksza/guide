require('../css/styles.css');
const introJs = require('intro.js');

const pluginName = 'guide';
const pluginLabel = 'Guide';

const pluginVersion = '1.0.0';

const settings = {
};

const globals = {
};

// ******************************************************************************
// ********************* PLUGIN REGISTRATION WITH MINERVA *********************
// ******************************************************************************

let $container;
let minervaProxy;
let pluginContainer;
let pluginContainerId;
let selectedInMap = undefined;


const register = function(_minerva) {

    // console.log('registering ' + pluginName + ' plugin');

    // $(".tab-content").css('position', 'relative');

    minervaProxy = _minerva;
    pluginContainer = $(minervaProxy.element);
    pluginContainerId = pluginContainer.attr('id');
    console.log('pluginContainer', pluginContainer);

    // console.log('minerva object ', minervaProxy);
    // console.log('project id: ', minervaProxy.project.data.getProjectId());
    // console.log('model id: ', minervaProxy.project.data.getModels()[0].modelId);

    initPlugin();
};

const unregister = function () {
    // console.log('unregistering ' + pluginName + ' plugin');

    // unregisterListeners();
    // return deHighlightAll();
};

const getName = function() {
    return pluginLabel;
};

const getVersion = function() {
    return pluginVersion;
};

/**
 * Function provided by Minerva to register the plugin
 */
minervaDefine(function (){
    return {
        register: register,
        unregister: unregister,
        getName: getName,
        getVersion: getVersion
        // ,minWidth: 300
        ,defaultWidth: 450
    }
});

function initPlugin () {
    registerListeners();
    const $container = initMainContainer();
    initMainPageStructure($container);
}

function registerListeners(){
    minervaProxy.project.map.addListener({
        dbOverlayName: "search",
        type: "onSearch",
        callback: searchListener
    });
}

function unregisterListeners() {
    minervaProxy.project.map.removeAllListeners();
}

// ****************************************************************************
// ********************* MINERVA INTERACTION*********************
// ****************************************************************************

function searchListener(entities) {
    // selectedInMap = undefined;
    // if (entities[0].length > 0 && entities[0][0].constructor.name === 'Alias') {
    //     selectedInMap = entities[0][0];
    // }
    // highlightSelectedInTable();
}

function deHighlightAll(){
    // return minervaProxy.project.map.getHighlightedBioEntities().then( highlighted => minervaProxy.project.map.hideBioEntity(highlighted) );
}

// ****************************************************************************
// ********************* PLUGIN STRUCTURE AND INTERACTION*********************
// ****************************************************************************

function getContainerClass() {
    return pluginName + '-container';
}

function initMainContainer(){
    $container = $(`<div class="${getContainerClass()}"></div>`).appendTo(pluginContainer);
    // $container.append(`
    // <div class="panel panel-default drugs-loading">
    //     <div class="panel-body">
    //         <i class="fa fa-circle-o-notch fa-spin"></i> Obtaining drug-bioentities mapping. This might take several minutes
    //         if this is the first time the plugin is loaded for this map ...
    //     </div>
    // </div>`);

    return $container;
}

function initMainPageStructure(){

    $container.append(`        
        <div class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Error</h4>
                    </div>
                    <div class="modal-body">
                        <p class="text-warning">Issue.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div> 
    `);

    intializeIntroJs();

    // container.find('.btn-search').on('click', () => search() );
    // container.find('.input-disease').keypress(e => {if (e.which == 13) {search(); return false}} );
}

function intializeIntroJs(){
    initializeIntroJsStory1();
}

function initializeIntroJsStoryHtml(id, headerText, description){
    return $container.append(`
        <div class="container-fluid"> 
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary story">
                        <div class="panel-heading">${headerText}
                        </div>
                        <div class="panel-body">                            
                            ${description}
                            <span class="pull-right glyphicon glyphicon-chevron-down clickable_space" data-toggle="collapse" data-target="#data_${id}">
                        </div>
                        <div id="data_${id}" class="panel-collapse collapse">
                            <ul class="list-group">                            
                            </ul>
                        </div>
                    </div>                               
                </div>
            </div>
        </div>`);
}

function initializeIntroStoryStepsHtml($introContainer, steps) {
    let i = 1;
    steps.forEach(step => {
        $introContainer.find('ul').append(`<li class="list-group-item">${i}. ${step.intro}</li>`);
        i+=1;
    })
}

function initializeIntroJsStory1Contet($introContainer){

    const intro = introJs();

    const steps = [
        { //0
            intro: "This tutorial will guide you through the basic MINERVA functionality!",
        },
        { //1
            element: $('.minerva-left-panel')[0],
            intro: "Meet the left panel.",
            scrollTo: $("#minervaAppDiv")
        },
        { //2
            element:  $('.minerva-left-panel .nav.nav-tabs:contains("SEARCH")')[0],
            intro: "Now meet the upper panel using which you can access the search functionality. Let's try it..."
        },
        { //3
            element: $('div[name="tabView"]:contains("GENERIC")')[1],
            intro: "Oh, snap. Near miss. This is the drug search.... Ok let's try again"
        },
        { //4
            element: $('div[name="tabView"]:contains("GENERIC")')[1],
            intro: "Here we go"
        },
        { //5
            element: $('div[name="tabView"]:contains("GENERIC")')[1],
            intro: "Now let's search for a gene. But which one? .... hmmm .... let's try SNCA, aka PARK1, aka PARK4, aka .... you got the idea. One of the most commonly blablabla"
        },
        { //6
            element: $('div[name="tabView"]:contains("GENERIC")')[1],
            intro: "First we enter the gene name into the search box"
        },
        { //7
            element: $('#panel_tab_1 a:has("img")')[0],
            intro: "And then we click the search button to get the results"
        },
        { //8
            element: $('div[name="searchResults"]')[0],
            intro: "Here we see the list containing the search results"
        },
        { //9
            element: $('.minerva-map')[0],
            intro: "And in the map the search results are highlighted by the icons"
        }
    ];

    intro.setOptions({
        steps: steps,
        showProgress: true,
        showBullets: false
    });
    initializeIntroStoryStepsHtml($introContainer, steps);

    intro.onchange(function (target) {
        $($introContainer.find('li')[this._currentStep]).addClass('list-group-item-success');
        //handle back button
        $introContainer.find('li').each((i, el) => {
            if (i > this._currentStep){
                $(el).removeClass('list-group-item-success');
            }
        })
    });

    intro.onbeforechange(function (target) {

        if (this._currentStep == 3){
            $('a:contains("DRUG")').click();
        } else if (this._currentStep == 4){
            $('a:contains("GENERIC")').click();
        }
    });

    intro.onafterchange(function (target) {

        if (this._currentStep == 7){
            $('#panel_tab_1 input[name="searchInput"]').val("PRKN")
        } else if (this._currentStep == 8){
            $('#panel_tab_1 a:has("img")').click();
        }

    });

    intro.onexit(function () {
        $('#minervaAppDiv').css('padding', '0px');
        //TODO the calc should be changed so that when the calc changes in the minerva code, the plugin still works correctly
        pluginContainer.closest('.tab-pane').css('height', 'calc(100vh - 42px)');
    });


    return intro;
}

function cleanStory($introContainer) {
    $introContainer.find('li').each((ix, val) => {
        $(val).removeClass('list-group-item-success');

    });
}

function cleanMinerava(){
    $('.minerva-overview-button:contains("CLEAR")').click();
    $($('div[name="tabView"]:contains("SEARCH") .nav:has(i.fa-search) a')[0]).click();
    $('a:contains("GENERIC")').click();
}

function startIntro($introContainer, intro, step = 0) {

    cleanMinerava();
    cleanStory($introContainer);
    //TODO the calc should be changed so that when the calc changes in the minerva code, the plugin still works correctly
    pluginContainer.closest('.tab-pane').css('height', 'calc(100vh - 42px - 30px)');
    $('#minervaAppDiv').css('padding', '30px');

    intro.start();
    for (let i = 0; i < step; i++) {
        intro.nextStep();
    }
}

function initializeIntroJsStory1() {
    const id = 'story1';
    const $introContainer = initializeIntroJsStoryHtml(id, 'First story', 'Story description');
    const intro = initializeIntroJsStory1Contet($introContainer);

    $introContainer.find('.panel-heading').click(() => {
        startIntro($introContainer, intro);
    });
    $introContainer.find('.panel.story li').each((i, el) => {
        $(el).click(() => startIntro($introContainer, intro, i) );
    })


}

function queryMinervaApi(query, parameters, queryType) {

    const apiAddress = ServerConnector.getApiBaseUrl();
    if (!queryType) queryType = 'GET';
    if (!parameters) parameters = '';

    return $.ajax({
        type: queryType,
        url: `${apiAddress}${query}`,
        // dataType: 'json',
        data: parameters
    })
}

function isString(s) {
    return typeof(s) === 'string' || s instanceof String;
}

function showModal() {
    pluginContainer.find(".modal").modal('show');
}